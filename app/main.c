#include "bsp.h"
#include "bsp_leds.h"
#include "mrfi.h"
#include "radios/family1/mrfi_spi.h"
#include "driverlib/sysctl.h"
volatile uint16_t x;
mrfiPacket_t packet;
int main()
{
	// Set the system clock to run at 50Mhz off PLL with external crystal as
	// reference.
	ROM_SysCtlClockSet(SYSCTL_SYSDIV_4 | SYSCTL_USE_PLL | SYSCTL_XTAL_16MHZ |
                       SYSCTL_OSC_MAIN);
	BSP_Init();
	MRFI_Init();
  mrfiSpiWriteReg(PA_TABLE0,0xC0);
	MRFI_WakeUp();
  MRFI_RxOn();
	packet.frame[0]=8+5;
	BSP_TURN_ON_LED1(); 
	while (1)
	{
		MRFI_DelayMs(200);
    //MRFI_Transmit(&packet, MRFI_TX_TYPE_FORCED);
		//BSP_TOGGLE_LED1();
	}
}

void MRFI_RxCompleteISR()
{
	//MRFI_Transmit(&packet, MRFI_TX_TYPE_FORCED);
	BSP_TOGGLE_LED2();
}
